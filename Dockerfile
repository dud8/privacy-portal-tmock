FROM openjdk:8-jre-alpine

ENV APP_JAR="/opt/ppm/lib/app.jar"

VOLUME ["/tmp", "/var/log"]

ADD target/app.jar /opt/ppm/lib/app.jar

EXPOSE 8080 8082

ENTRYPOINT ["java", "-jar", "/opt/ppm/lib/app.jar"]