import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {LogsService} from '../../service/logs.service';
import {Logs} from '../../model/logs';

@Component({
  selector: 'logs-root',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css'],
  providers: [LogsService]
})


export class LogsComponent implements OnInit {

  logs: Logs[] = [];
  appUri: string = location.href;

  constructor(private logsService: LogsService) {
  }

  public ngOnInit() {
    // let page = this.activatedRoute.snapshot.queryParams["page"];
    //  let size = this.activatedRoute.snapshot.queryParams["size"];
    //  let sort = this.activatedRoute.snapshot.queryParams["sort"];
    //  console.log(RouterModule.forRoot())
    //  this.route.queryParams
    //    .filter(params => params.order)
    //    .subscribe(params => {
    //      console.log(params); // {order: "popular"}
    //
    //      let order = params.order;
    //      console.log(order); // popular
    //    });
    let page = 0, size = 100, sort = "timestamp,desc";

    this.updateLogs(4, page, size, sort);
  }

  /*
  page	Page you want to retrieve.
size	Size of the page you want to retrieve.
sort	Properties that should be sorted by in the format property,property(,ASC|DESC). Default sort direction is ascending. Use multiple sort parameters if you want to switch directions, e.g. ?sort=firstname&sort=lastname,asc.

   */

  private updateLogs(delay: number, page: number, size: number, sort: string) {


    Observable.interval(delay * 1000).subscribe(x => {
      this.logsService
        .getAllLogs(page, size, sort)
        .subscribe(
          (logs) => {
            this.logs = logs;
          }
        );
    });
  }
}

