export class Logs {
  id: number;
  request: string;
  response: string;
  timestamp: Date;
  service: string;

constructor(id: number, request: string, response: string, timestamp: Date, service: string) {
    this.id = id;
    this.request = request;
    this.response = response;
    this.timestamp = timestamp;
    this.service = service;
  }
}
