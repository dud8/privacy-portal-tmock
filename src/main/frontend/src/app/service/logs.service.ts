import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {Logs} from '../model/logs';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from "@angular/common/http";

const API_URL = environment.apiUrl;

@Injectable()
export class LogsService {

  constructor(private api: HttpClient) {
  }

  getAllLogs(page, size, sort): Observable<Logs[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('page', page);
    httpParams = httpParams.set('size', size);
    httpParams = httpParams.set('sort', sort);
    return this.api.get<Logs[]>(API_URL + "log", {params: httpParams});
  }
}
