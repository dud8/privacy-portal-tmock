import {Location} from "@angular/common";

export const environment = {
  production: true,

  // URL of development API
  apiUrl: location.href
};
