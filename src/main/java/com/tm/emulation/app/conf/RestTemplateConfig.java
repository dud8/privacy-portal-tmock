package com.tm.emulation.app.conf;

import java.util.Arrays;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Value ("${this.rest.connectTimeout}")
    private int connectTimeout;

    @Value ("${this.rest.readTimeout}")
    private int readTimeout;

    @Value ("${this.rest.avoidSslValidation}")
    private boolean avoidSslValidation;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        if (avoidSslValidation) { return getRestTemplateAvoidSSLValidation(); } else {
            return builder.setConnectTimeout(connectTimeout).setReadTimeout(readTimeout).build();
        }
    }


    private RestTemplate getRestTemplateAvoidSSLValidation() {
        HttpComponentsClientHttpRequestFactory httpClientFactory = new HttpComponentsClientHttpRequestFactory();
        SSLContext sslContext = null;
        try {
            sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, (hostname, session) -> true);

        httpClientFactory.setHttpClient(HttpClients.custom().setSSLSocketFactory(sslsf).build());
        httpClientFactory.setReadTimeout(readTimeout);
        httpClientFactory.setConnectTimeout(connectTimeout);

        RestTemplate restTemplate = new RestTemplate(httpClientFactory);
        restTemplate.getMessageConverters().addAll(Arrays.asList(new MappingJackson2HttpMessageConverter(), new StringHttpMessageConverter()));

        return restTemplate;
    }

}
