package com.tm.emulation.controller;

import com.google.common.base.Joiner;
import com.tm.emulation.exceptions.ErrorCoded;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {

    public static Map<String, String> createErrorResponse(ErrorCoded ex) {
        return createErrorResponse(ex.getErrorCode(), ex.getMessage());
    }

    public static Map<String, String> createErrorResponse(String code, String msg) {
        Map<String, String> response = new HashMap<>();
        response.put("error_code", code);
        response.put("error_msg", msg);
        return response;
    }

    @ExceptionHandler (Exception.class)
    public ResponseEntity handleError(HttpServletRequest req, Exception ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex, ex);
        List<String> errorMsg = new ArrayList<>();
        Throwable exception = ex;
        do {
            errorMsg.add(exception.getMessage());
            exception = exception.getCause();
        } while (exception != null);
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(createErrorResponse("100", Joiner.on(" -> ").join(errorMsg)));
    }
}