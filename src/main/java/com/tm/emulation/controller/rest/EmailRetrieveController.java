package com.tm.emulation.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.tm.emulation.entity.TmSystemEntity;
import com.tm.emulation.model.RetrievedData;
import com.tm.emulation.repository.TmSystemRepository;
import com.tm.emulation.service.LogService;
import com.tm.emulation.service.RetrieveCustomerDataService;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.tm.emulation.controller.GlobalControllerExceptionHandler.createErrorResponse;
import static com.tm.emulation.service.RetrieveCustomerDataService.EMAIL_PARAM;
import static com.tm.emulation.service.RetrieveCustomerDataService.SOURCE_PARAM;
import static com.tm.emulation.service.RetrieveCustomerDataService.SYSTEM_PARAM;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping("/rest")
@Slf4j
@AllArgsConstructor
public class EmailRetrieveController {

    public static final String API_KEY_HEADER_NAME = "x-api-key";
    private final ObjectMapper mapper;
    private final LogService logService;
    private final TmSystemRepository tmSystemRepository;
    private final RetrieveCustomerDataService retrieveCustomerDataService;


    @PostMapping(value = "/retrieveCustomerData")
    public ResponseEntity retrieveCustomerData(@RequestBody Map<String, String> param, HttpServletRequest httpServletRequest) throws Exception {
        String email = param.get(EMAIL_PARAM).trim();
        String source = param.get(SOURCE_PARAM).toUpperCase().trim();

        String tmSystemName = httpServletRequest.getParameter(SYSTEM_PARAM);
        String apiKey = httpServletRequest.getHeader(API_KEY_HEADER_NAME);

        ResponseEntity responseEntity = validateToken(apiKey, tmSystemName);
        if (responseEntity == null) {
            String error = httpServletRequest.getParameter("error");
            if (error == null) {
                if (email.contains("ererror")) {
                    responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Email retrieve error emulated");
                } else {
                    final RetrievedData retrievedData = retrieveCustomerDataService.findByEmailAndSystem(email, tmSystemName, source);
                    responseEntity = ResponseEntity.ok(retrievedData);
                }
            } else {
                responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body("{\"error\":\"error description: " + error + "\"}");
            }
        }

        //        String responseString="{\"email_found_flag\": true, \"found_customers\": [{\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167763\", \"SubscriberKey\": \"012-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167763\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:52:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167763\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:52:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167764\", \"SubscriberKey\": \"013-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167764\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:52:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167764\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:52:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167765\", \"SubscriberKey\": \"014-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167765\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167765\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167768\", \"SubscriberKey\": \"015-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167768\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167768\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167770\", \"SubscriberKey\": \"016-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167770\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167770\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167772\", \"SubscriberKey\": \"017-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167772\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167772\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:53:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167773\", \"SubscriberKey\": \"018-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167773\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167773\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167785\", \"SubscriberKey\": \"019-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167785\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167785\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}, {\"data_admin_config\": {\"data_controller_tm_flag\": true, \"market\": \"GB\", \"data_controller_id\": -1, \"data_controller_name\": \"N/A - TEST PLATFORM\"}, \"customer_identifier\": {\"EnterpriseAccountID\": \"1081651\", \"SubscriberID\": \"1158167786\", \"SubscriberKey\": \"020-mftest@tm.co.uk\"}, \"personal_details\": {\"ListMembership\": [{\"SubscriberId\": \"1158167786\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2012-09-28T13:20:15.56\", \"ListId\": \"3476\", \"ListName\": \"All Subscribers\"}, {\"SubscriberId\": \"1158167786\", \"Status\": \"Active\", \"CreatedDate\": \"2018-02-28T11:54:00\", \"ModifiedDate\": \"2018-02-23T05:28:38.98\", \"ListId\": \"355332\", \"ListName\": \"DeletionTesting\"}]}, \"date_latest_activity\": \"2018-03-02T14:02:54.905Z\"}]}";
        //
        //        responseString="{\n"
        //                       + "    \"email_found_flag\": true,\n"
        //                       + "    \"found_customers\": [\n"
        //                       + "        {\n"
        //                       + "            \"data_admin_config\": {\n"
        //                       + "                \"market\": \"IE\",\n"
        //                       + "                \"data_controller_tm_flag\": false,\n"
        //                       + "                \"data_controller_id\": \"TM-IE\",\n"
        //                       + "                \"data_controller_name\": \"Ticketmaster Ireland\"\n"
        //                       + "            },\n"
        //                       + "            \"customer_identifier\": {\n"
        //                       + "                \"customerNameId\": 5330\n"
        //                       + "            },\n"
        //                       + "            \"personal_details\": [\n"
        //                       + "                {\n"
        //                       + "                    \"customerNameId\": \"5330\",\n"
        //                       + "                    \"acctSequenceId\": \"1\",\n"
        //                       + "                    \"tableName\": \"Summary\",\n"
        //                       + "                    \"sequenceId\": \"1\",\n"
        //                       + "                    \"tag\": \"acct_id|cust_name_id|cust_type|date_last_payment|total_purchase_due|total_paid_due|total_payment_due\",\n"
        //                       + "                    \"value\": \"128983|5330|Primary user||||\",\n"
        //                       + "                    \"addDatetime\": \"2018-03-20 09:02:06.973\"\n"
        //                       + "                },\n"
        //                       + "                {\n"
        //                       + "                    \"customerNameId\": \"5330\",\n"
        //                       + "                    \"acctSequenceId\": \"1\",\n"
        //                       + "                    \"tableName\": \"t_cust\",\n"
        //                       + "                    \"sequenceId\": \"1\",\n"
        //                       + "                    \"tag\": \"acct_id|rec_status|acct_type|acct_rep_type|tag|since_date|add_user|add_datetime|upd_user|upd_datetime|priority|source|source_desc|referral_acct_id|solicit_allowed|solicit_frequency|anonymous_donor|honorary_donor_level|target_donation_amount|ticket_purchases|ticket_payments|sc_purchases|sc_payments|donations_pledged|donations_received|on_account_payments|misc_credits|charge_back_amount|charge_back_count|invoice_balance|donations_hpc_amount|donations_hpc_date|donations_mrc_amount|donations_mrc_date|donations_itd_amount|donations_itd_count|other_info_1|other_info_2|other_info_3|other_info_4|other_info_5|other_info_6|other_info_7|other_info_8|other_info_9|other_info_10|source_list_id|user_id|activity_level|points_ytd|points_itd|default_patron|acct_id_deeplink|other_info_11|other_info_12|other_info_13|other_info_14|other_info_15|other_info_16|other_info_17|other_info_18|other_info_19|other_info_20|rank\",\n"
        //                       + "                    \"value\": \"128983|A|P|N||2016-01-15|tm53|2016-01-15 15:37:16.697|TM432|2017-03-08 11:30:30.618||A|||Y||N|||0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0|0.00|0.00||0.00||0.00|0|||||||||||||0|0.00|0.00||caafcafjagbagbafhaf|||||||||||128983\",\n"
        //                       + "                    \"addDatetime\": \"2018-03-20 09:02:04.864\"\n"
        //                       + "                }, {\n"
        //                       + "                    \"customerNameId\": \"5330\",\n"
        //                       + "                    \"acctSequenceId\": \"1\",\n"
        //                       + "                    \"tableName\": \"t_cust_trace\",\n"
        //                       + "                    \"sequenceId\": \"15837\",\n"
        //                       + "                    \"tag\": \"seq_id|acct_id|activity_code|call_reason|rc|error_desc|ip_address|add_datetime|add_user|cust_name_id|activity_comment\",\n"
        //                       + "                    \"value\": \"154833|128983|CU||0|||2017-11-14 14:37:35.225|tm57|5330|\",\n"
        //                       + "                    \"addDatetime\": \"2018-03-20 09:02:05.036\"\n"
        //                       + "                }\n"
        //                       + "            ],\n"
        //                       + "            \"date_latest_activity\": \"2017-11-14T19:37:35.225Z\",\n"
        //                       + "            \"date_latest_even\": null\n"
        //                       + "        }\n"
        //                       + "    ]\n"
        //                       + "}";
        //        responseEntity = ResponseEntity.ok(responseString);

//        responseEntity = ResponseEntity.ok("{\n" +
//                "    \"email_found_flag\": true,\n" +
//                "    \"found_customers\": [\n" +
//                "        {\n" +
//                "            \"customer_identifier\": {\n" +
//                "                \"Email\": \"email@email.com\",\n" +
//                "                \"Ids\": [\n" +
//                "                    15\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            \"personal_details\": {\n" +
//                "                \"Data\": [\n" +
//                "                    {\n" +
//                "                        \"ID\": 15,\n" +
//                "                        \"EvCode\": \"ET\",\n" +
//                "                        \"EventName\": \"ETT\",\n" +
//                "                        \"EpDate\": \"2017-10-11T00:00:00.000Z\",\n" +
//                "                        \"TCODE\": \"UMY\",\n" +
//                "                        \"ShippingCountry\": null,\n" +
//                "                        \"Project_Id\": \"1\",\n" +
//                "                        \"Language\": \"UA\",\n" +
//                "                        \"Currency\": \"UAH\",\n" +
//                "                        \"billing_phone_number1\": null,\n" +
//                "                        \"shipping_tax\": 2,\n" +
//                "                        \"billingcountry\": null,\n" +
//                "                        \"VenueName\": \"Ticketmaster\",\n" +
//                "                        \"TransDate\": \"2017-10-11T00:00:00.000Z\",\n" +
//                "                        \"SalesChannel\": \"Internet\",\n" +
//                "                        \"contact_title\": null,\n" +
//                "                        \"contact_first_name\": \"IAN\",\n" +
//                "                        \"contact_middle_initial\": null,\n" +
//                "                        \"contact_last_name\": \"HANDSON\",\n" +
//                "                        \"Email\": \"email@email.com\",\n" +
//                "                        \"NoEmailFlag\": null,\n" +
//                "                        \"customer_account_status\": null,\n" +
//                "                        \"Vaxacct\": \"13-43296\",\n" +
//                "                        \"contact_phone_number1\": null,\n" +
//                "                        \"contact_phone_number2\": null,\n" +
//                "                        \"contact_suffix\": null,\n" +
//                "                        \"address1\": null,\n" +
//                "                        \"Address2\": null,\n" +
//                "                        \"address_City\": null,\n" +
//                "                        \"State\": null,\n" +
//                "                        \"address_state\": null,\n" +
//                "                        \"address_zip\": null,\n" +
//                "                        \"shipping1\": null,\n" +
//                "                        \"Shipping2\": null,\n" +
//                "                        \"shipping_City\": null,\n" +
//                "                        \"notused\": null,\n" +
//                "                        \"shipping_state\": null,\n" +
//                "                        \"shipping_zip\": null,\n" +
//                "                        \"unique_id\": null,\n" +
//                "                        \"price_Per\": 10,\n" +
//                "                        \"Quantity\": 10,\n" +
//                "                        \"Shipping\": 0,\n" +
//                "                        \"Tax\": 0,\n" +
//                "                        \"Artist\": null,\n" +
//                "                        \"Format\": null,\n" +
//                "                        \"CatalogueNumber\": \"44\",\n" +
//                "                        \"BarCode\": null,\n" +
//                "                        \"eventCode\": null,\n" +
//                "                        \"SeatType\": null,\n" +
//                "                        \"IsValidated\": true,\n" +
//                "                        \"IsSentToClient\": false,\n" +
//                "                        \"Partner_order_id\": null,\n" +
//                "                        \"Filename\": null,\n" +
//                "                        \"ProductTypeName\": null,\n" +
//                "                        \"Venue\": null,\n" +
//                "                        \"FullVenueName\": null,\n" +
//                "                        \"IsDeleted\": false,\n" +
//                "                        \"VenueTown\": null,\n" +
//                "                        \"EventTime\": null,\n" +
//                "                        \"SalesCompany\": null,\n" +
//                "                        \"IsRestricted\": true\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            \"date_latest_event\": \"2017-10-11T00:00:00.000Z\",\n" +
//                "            \"date_latest_activity\": null,\n" +
//                "            \"data_admin_config\": {\n" +
//                "                \"data_controller_tm_flag\": true,\n" +
//                "                \"market\": \"GB\",\n" +
//                "                \"data_controller_id\": \"TM-UK\",\n" +
//                "                \"data_controller_name\": \"TM-UK\"\n" +
//                "            }\n" +
//                "        }\n" +
//                "    ]\n" +
//                "}");


        String requestValue = param.toString() + "\nApi key:" + apiKey + "\nTmSystem:" + tmSystemName;
        logService.saveLog(requestValue, mapper.writeValueAsString(responseEntity),
                httpServletRequest.getRemoteHost() + "\n=>\nEMAIL RETRIEVE");
        return responseEntity;
    }

    private ResponseEntity validateToken(String apiKey, String tmSystemName) {
        final TmSystemEntity tmSystem = tmSystemRepository.findByName(tmSystemName);


        if (tmSystem == null) {
            throw new RuntimeException("Tm System: '" + tmSystemName + "' not found");
        }

        try {// imitate timeout
            Thread.sleep(tmSystem.getErTimeout());
        } catch (InterruptedException e) {
            log.warn(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }

        //check of api key
        if (!tmSystem.getMockApiKey().isEmpty()) {
            if (Strings.isNullOrEmpty(apiKey)) {
                return ResponseEntity.status(BAD_REQUEST)
                               .body(createErrorResponse("1", "Api-key was not provided"));
            } else if (!tmSystem.getMockApiKey().equals(apiKey)) {
                return ResponseEntity.status(BAD_REQUEST)
                               .body(createErrorResponse("3", "Api-key '" + apiKey + "' is invalid"));
            }
        }

        return null;
    }
}
