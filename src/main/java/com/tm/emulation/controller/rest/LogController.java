package com.tm.emulation.controller.rest;

import com.tm.emulation.entity.AppLogEntity;
import com.tm.emulation.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ("/log")
public class LogController {

    @Autowired
    private LogRepository logRepository;

    @GetMapping
    public List<AppLogEntity> getLog(Pageable pageable) {
        return logRepository.findAll(pageable).getContent();
    }

    @PostMapping
    public void saveLog(@RequestBody AppLogEntity log) {
        logRepository.save(log);
    }
}
