package com.tm.emulation.controller.rest;

import com.tm.emulation.exceptions.ApiKeyInvalidException;
import com.tm.emulation.exceptions.ApiKeyIsAbsentException;
import com.tm.emulation.exceptions.CustomerNotFoundException;
import com.tm.emulation.exceptions.PRInitErrorException;
import com.tm.emulation.service.PrivacyRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

import static com.tm.emulation.controller.GlobalControllerExceptionHandler.createErrorResponse;
import static com.tm.emulation.controller.rest.EmailRetrieveController.API_KEY_HEADER_NAME;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/privacyRequestDeliver")
@Slf4j
public class PrivacyRequestController {

    private final PrivacyRequestService privacyRequestService;

    public PrivacyRequestController(PrivacyRequestService privacyRequestService) {
        this.privacyRequestService = privacyRequestService;
    }

    @PostMapping
    public ResponseEntity handlePrivacyRequestDeliver(@RequestBody String in, HttpServletRequest httpServletRequest) throws ParseException {
        String error = httpServletRequest.getParameter("error");
        String apiKey = httpServletRequest.getHeader(API_KEY_HEADER_NAME);
        log.info("Processing PRIVACY REQUEST: " + in);

        if (error == null) {
            privacyRequestService.processPrivacyRequest(in, error, httpServletRequest.getRemoteHost() + " => PRIVACY REQUEST", apiKey);
            return ResponseEntity.ok("");
        } else {
            return ResponseEntity.status(BAD_REQUEST).body(createErrorResponse("2", error));
        }
    }


    @ExceptionHandler(ApiKeyIsAbsentException.class)
    public ResponseEntity handleError(HttpServletRequest req, ApiKeyIsAbsentException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);
        return ResponseEntity.status(BAD_REQUEST).body(createErrorResponse(ex.getErrorCode(), "Api-key is absent"));
    }

    @ExceptionHandler(ApiKeyInvalidException.class)
    public ResponseEntity handleError(HttpServletRequest req, ApiKeyInvalidException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);
        return ResponseEntity.status(BAD_REQUEST).body(createErrorResponse(ex.getErrorCode(), ex.getMessage()));
    }

    @ExceptionHandler(CustomerNotFoundException.class)
    public ResponseEntity handleError(HttpServletRequest req, CustomerNotFoundException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);
        return ResponseEntity.status(NOT_FOUND).body(createErrorResponse(ex.getErrorCode(), ex.getMessage()));
    }

    @ExceptionHandler(PRInitErrorException.class)
    public ResponseEntity handleError(HttpServletRequest req, PRInitErrorException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);
        return ResponseEntity.status(NOT_FOUND).body(createErrorResponse(ex.getErrorCode(), ex.getMessage()));
    }

}
