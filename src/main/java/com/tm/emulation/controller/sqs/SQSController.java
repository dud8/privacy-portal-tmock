package com.tm.emulation.controller.sqs;

import com.tm.emulation.controller.GlobalControllerExceptionHandler;
import com.tm.emulation.exceptions.ApiKeyInvalidException;
import com.tm.emulation.exceptions.ApiKeyIsAbsentException;
import com.tm.emulation.service.PrivacyRequestService;
import java.text.ParseException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/sqs")
@AllArgsConstructor
@Slf4j
public class SQSController {

    private final PrivacyRequestService privacyRequestService;


    @SqsListener (value = "${this.sqs.privacy-request-initiate}")
    private void receivePrivacyRequest(String request) throws ParseException {
        try {
            privacyRequestService.processPrivacyRequest(request, null, "SQS => PRIVACY REQUEST", null);
        } catch (ApiKeyInvalidException | ApiKeyIsAbsentException e) {
            privacyRequestService.sendToEverywhere(request, GlobalControllerExceptionHandler.createErrorResponse(e));
        } catch (Exception e) {
            privacyRequestService.sendToEverywhere(request, GlobalControllerExceptionHandler.createErrorResponse("100", e.getMessage()));
        }
    }
}