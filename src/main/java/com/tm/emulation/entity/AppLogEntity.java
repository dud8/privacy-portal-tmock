package com.tm.emulation.entity;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table (name = "app_log")
public class AppLogEntity {
    @Id
    @Column (name = "al_id", nullable = false)
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (name = "al_timestamp", insertable = false)
    @Temporal (TemporalType.TIMESTAMP)
    private Date timestamp;
    @Column (name = "al_request")
    private String request;
    @Column (name = "al_response")
    private String response;
    @Column (name = "al_service")
    private String service;
}
