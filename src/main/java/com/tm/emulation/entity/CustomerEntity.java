package com.tm.emulation.entity;


import com.tm.emulation.enums.RequestType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table (name = "customer")
public class CustomerEntity {

    @Id
    @Column (name = "IDENTIFIER")
    private String id;

    @Column (name = "EMAIL")
    private String email;

    @Column (name = "PERSONAL_DATA")
    private String personalData;

    @Column (name = "STATUS")
    @Enumerated (EnumType.STRING)
    private RequestType status;

    @Column (name = "LATEST_ACTION")
    @Temporal (TemporalType.TIMESTAMP)
    private Date latestAction;

    @Column (name = "LATEST_EVENT")
    @Temporal (TemporalType.TIMESTAMP)
    private Date latestEvent;

    @ManyToOne
    @JoinColumn (name = "DC_ID", referencedColumnName = "ID")
    private DataControllerEntity dataController;

}
