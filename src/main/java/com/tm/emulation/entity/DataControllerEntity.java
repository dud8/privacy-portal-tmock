package com.tm.emulation.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Setter
@Getter
@Entity
@Table (name = "data_controller")
@ToString
public class DataControllerEntity {

    @Id
    @Column (name = "ID")
    private Long id;

    @OneToMany (mappedBy = "dataController", cascade = CascadeType.REMOVE)
    private List<CustomerEntity> customers;

    @Column (name = "CLIENT_NAME")
    private String name;

    @Column (name = "CLIENT_ID")
    private String clientId;

    @Column (name = "MARKET")
    private String market;

    @ManyToOne
    @JoinColumn (name = "TM_SYSTEM_ID", referencedColumnName = "ID")
    private TmSystemEntity tmSystem;

    @Column (name = "CLIENT_FLAG")
    private String tmClientFlag;

}
