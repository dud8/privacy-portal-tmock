package com.tm.emulation.entity;

import com.tm.emulation.enums.ResponseType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Setter
@Getter
@Entity
@Table (name = "tm_system")
public class TmSystemEntity {

    @Id
    @Column (name = "ID")
    private Long id;

    @Column (name = "NAME")
    private String name;

    @Column (name = "ER_TIMEOUT")
    private Long erTimeout;

    @Column(name = "PR_UPDATE")
    private Long prResponseTimeout;

    @Column(name = "PR_INIT")
    private Long prRequestTimeout;

    @Column (name = "RESPONSE_ADDRESS")
    private String responseAddress;

    @Column (name = "MOCK_API_KEY")
    private String mockApiKey;

    @Column (name = "PP_API_KEY")
    private String ppApiKey;

    @OneToMany (mappedBy = "tmSystem", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DataControllerEntity> controllers;

    public ResponseType getResponseType() {
        if (responseAddress.startsWith("http://") || responseAddress.startsWith("https://")) {
            return ResponseType.API;
        } else {
            return ResponseType.SQS;
        }
    }
}
