package com.tm.emulation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RequestState {
    SUBMITTED(1),
    TIMEOUT(2),
    FAILED(3),
    COMPLETE(4),
    REJECTED(5);

    private final Integer id;
}
