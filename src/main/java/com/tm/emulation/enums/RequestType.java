package com.tm.emulation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RequestType {
    ERASE(1),
    FULL_RESTRICT(2),
    PARTIAL_RESTRICT(3),
    UNRESTRICT(4),
    NORMAL(5);

    private final Integer id;
}

