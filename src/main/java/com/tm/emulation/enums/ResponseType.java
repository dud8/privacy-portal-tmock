package com.tm.emulation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseType {
    SQS(1),
    API(2);

    private final Integer id;
}
