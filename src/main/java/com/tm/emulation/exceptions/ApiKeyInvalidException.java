package com.tm.emulation.exceptions;

public class ApiKeyInvalidException extends RuntimeException implements ErrorCoded {

    public ApiKeyInvalidException(final String message) {
        super(message);
    }

    @Override
    public String getErrorCode() {
        return "1";
    }
}

