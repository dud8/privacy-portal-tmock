package com.tm.emulation.exceptions;

public class ApiKeyIsAbsentException extends RuntimeException implements ErrorCoded {

    public ApiKeyIsAbsentException() {
        super("Api-key was not provided)");
    }

    @Override
    public String getErrorCode() {
        return "3";
    }
}
