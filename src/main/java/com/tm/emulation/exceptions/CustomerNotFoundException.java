package com.tm.emulation.exceptions;

import com.tm.emulation.model.privacyportal.request.Request;


public class CustomerNotFoundException extends RequestException {

    public CustomerNotFoundException(final String message, final Request request) {
        super(message, request);

    }

    @Override
    public String getErrorCode() {
        return null;
    }
}
