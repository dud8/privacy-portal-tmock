package com.tm.emulation.exceptions;

public interface ErrorCoded {
    String getErrorCode();

    String getMessage();
}
