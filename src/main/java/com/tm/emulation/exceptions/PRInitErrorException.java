package com.tm.emulation.exceptions;

public class PRInitErrorException extends RuntimeException implements ErrorCoded {

    public PRInitErrorException(final String message) {
        super(message);
    }

    @Override
    public String getErrorCode() {
        return "10";
    }
}

