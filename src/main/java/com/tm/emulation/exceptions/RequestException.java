package com.tm.emulation.exceptions;

import com.tm.emulation.model.privacyportal.request.Request;
import lombok.Getter;


public abstract class RequestException extends RuntimeException implements ErrorCoded {
    @Getter
    protected Request request;

    public RequestException(final String message, final Request request) {
        super(message);
        this.request = request;
    }
}
