package com.tm.emulation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tm.emulation.util.DateSerializer;
import com.tm.emulation.util.StringAsObjectJsonSerializer;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;


@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @JsonProperty ("data_admin_config")
    private DataAdminConfig dataAdminConfig;

    @JsonProperty ("customer_identifier")
    @JsonSerialize (using = StringAsObjectJsonSerializer.class)
    private String customerIdentifier;

    @JsonProperty ("personal_details")
    @JsonSerialize (using = StringAsObjectJsonSerializer.class)
    private String personalData;

    @JsonProperty ("date_latest_activity")
    @JsonSerialize (using = DateSerializer.class)
    private Date latestAction;

    @JsonProperty ("date_latest_event")
    @JsonSerialize (using = DateSerializer.class)
    private Date latestEvent;
}
