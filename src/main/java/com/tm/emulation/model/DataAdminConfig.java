package com.tm.emulation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataAdminConfig {
    @JsonProperty ("data_controller_tm_flag")
    private boolean tmFlag;
    @JsonProperty ("market")
    private String market;
    @JsonProperty ("data_controller_id")
    private String controllerId;
    @JsonProperty ("data_controller_name")
    private String controllerName;
}
