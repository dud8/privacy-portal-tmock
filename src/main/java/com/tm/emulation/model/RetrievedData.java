package com.tm.emulation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetrievedData {
    @JsonProperty ("email_found_flag")
    private Boolean emailFoundFlag;
    @JsonProperty ("found_customers")
    private List<Customer> foundCustomers = new ArrayList<>();
}
