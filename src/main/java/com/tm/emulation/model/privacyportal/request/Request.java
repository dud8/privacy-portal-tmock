package com.tm.emulation.model.privacyportal.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tm.emulation.enums.RequestType;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Request {
    @JsonProperty ("request_id")
    private UUID id;
    @JsonProperty ("request_type")
    private RequestType requestType;
    @JsonProperty ("customer_identifier")
    private String customerIdentifier;
}
