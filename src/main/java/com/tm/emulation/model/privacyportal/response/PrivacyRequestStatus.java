package com.tm.emulation.model.privacyportal.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PrivacyRequestStatus {
    @JsonProperty ("status_updates")
    List<StatusUpdate> statusUpdates;
}
