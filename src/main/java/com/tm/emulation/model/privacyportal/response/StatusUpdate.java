package com.tm.emulation.model.privacyportal.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tm.emulation.enums.RequestState;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@ToString
public class StatusUpdate {
    @JsonProperty ("request_id")
    private UUID requestId;
    @JsonProperty ("request_status")
    private RequestState requestStatus;
    @JsonProperty ("comment")
    private String comment;
    @JsonProperty ("timestamp")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestamp;
}
