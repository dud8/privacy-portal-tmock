package com.tm.emulation.repository;

import com.tm.emulation.entity.CustomerEntity;
import com.tm.emulation.enums.RequestType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, String> {
    CustomerEntity findByEmail(String email);

    @Query ("select cst from CustomerEntity cst "
            + "join cst.dataController dc "
            + "join dc.tmSystem tms "
            + "where cst.email=:email and tms.name=:tmSystem and dc.tmClientFlag=:source and cst.status<>'ERASE'")
    List<CustomerEntity> findByEmailAndTmSystemAndSource(@Param ("email") String email,
                                                         @Param ("tmSystem") String tmSystem,
                                                         @Param ("source") String source);

    List<CustomerEntity> findByEmailAndDataController_TmSystem_NameAndStatusNot(String email, String tmSystemName, RequestType forbiddenStatus);
}
