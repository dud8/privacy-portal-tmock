package com.tm.emulation.repository;

import com.tm.emulation.entity.TmSystemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TmSystemRepository extends JpaRepository<TmSystemEntity, Long> {
    TmSystemEntity findByName(String name);
}
