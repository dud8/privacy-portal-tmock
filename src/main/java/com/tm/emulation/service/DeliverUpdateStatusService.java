package com.tm.emulation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tm.emulation.entity.TmSystemEntity;
import com.tm.emulation.enums.ResponseType;
import com.tm.emulation.model.privacyportal.response.PrivacyRequestStatus;
import com.tm.emulation.service.privacyrequest.DeliverUpdateStatusApiService;
import com.tm.emulation.service.privacyrequest.DeliverUpdateStatusSqsService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class DeliverUpdateStatusService {

    private final DeliverUpdateStatusApiService updateStatusApi;
    private final DeliverUpdateStatusSqsService updateStatusSqs;

    public DeliverUpdateStatusService(DeliverUpdateStatusApiService updateStatusApi, DeliverUpdateStatusSqsService updateStatusSqs) {
        this.updateStatusApi = updateStatusApi;
        this.updateStatusSqs = updateStatusSqs;
    }

    public ResponseEntity deliverUpdateStatus(TmSystemEntity tmSystemEntity, PrivacyRequestStatus privacyRequestStatus) throws JsonProcessingException {
        return getUpdateStatusDeliver(tmSystemEntity).deliver(tmSystemEntity, privacyRequestStatus);
    }

    private PrivacyRequestUpdateStatusDeliver getUpdateStatusDeliver(TmSystemEntity tmSystemEntity) {
        ResponseType privacyRequestResponseType = tmSystemEntity.getResponseType();

        if (privacyRequestResponseType == ResponseType.API) {
            return updateStatusApi;
        } else if (privacyRequestResponseType == ResponseType.SQS) {
            return updateStatusSqs;
        } else {
            throw new RuntimeException("Wrong privacy request response type. valid response type API, SQS");
        }
    }

    public interface PrivacyRequestUpdateStatusDeliver {
        ResponseEntity deliver(TmSystemEntity tmSystemEntity, PrivacyRequestStatus privacyRequestStatus) throws JsonProcessingException;
    }
}
