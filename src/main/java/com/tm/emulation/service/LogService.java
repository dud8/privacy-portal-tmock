package com.tm.emulation.service;

import com.tm.emulation.entity.AppLogEntity;
import com.tm.emulation.repository.LogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class LogService {

    @Autowired
    private LogRepository logRepository;

    @Transactional
    public void saveLog(String request, String response, String service) {
        log.info("Req: {};\n====                ===\n Resp:{};\n====           ===\n Service: {}", request, response, service);
        AppLogEntity log = new AppLogEntity();
        log.setRequest(request);
        log.setResponse(response);
        log.setService(service);
        logRepository.save(log);
    }
}
