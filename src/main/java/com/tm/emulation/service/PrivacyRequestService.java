package com.tm.emulation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tm.emulation.controller.GlobalControllerExceptionHandler;
import com.tm.emulation.entity.CustomerEntity;
import com.tm.emulation.enums.RequestState;
import com.tm.emulation.enums.RequestType;
import com.tm.emulation.exceptions.ApiKeyInvalidException;
import com.tm.emulation.exceptions.ApiKeyIsAbsentException;
import com.tm.emulation.exceptions.CustomerNotFoundException;
import com.tm.emulation.exceptions.PRInitErrorException;
import com.tm.emulation.model.privacyportal.request.PrivacyRequest;
import com.tm.emulation.model.privacyportal.request.Request;
import com.tm.emulation.model.privacyportal.response.PrivacyRequestStatus;
import com.tm.emulation.model.privacyportal.response.StatusUpdate;
import com.tm.emulation.repository.CustomerRepository;
import com.tm.emulation.repository.TmSystemRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PrivacyRequestService {

    private final CustomerRepository customerRepository;
    private final DeliverUpdateStatusService deliverUpdateStatusService;
    private final LogService logService;
    private final TmSystemRepository tmSystemRepository;
    private final ObjectMapper mapper;


    public void processPrivacyRequest(String in, String error, String serviceUrl, final String apiKey) throws ParseException {
        log.info("Process Privacy request {}", in);
        PrivacyRequest request = convert(in);

        request.getRequests().stream().filter(req -> customerRepository.findOne(req.getCustomerIdentifier()) != null)
                .forEach(req -> {
                    String errorMsg;
                    CustomerEntity customerEntity = customerRepository.findOne(req.getCustomerIdentifier());
                    String responseString = "ApiKey:" + apiKey + " -> \n" + req.toString();
                    if (customerEntity == null) {
                        errorMsg = "Customer '" + req.getCustomerIdentifier() + "' not found";
                        logService.saveLog(responseString, errorMsg, serviceUrl);
                        throw new CustomerNotFoundException(errorMsg, req);
                    } else {
                        responseString += "\nTmSystem:" + customerEntity.getDataController().getTmSystem().getName();
                    }


                    try {// imitate timeout
                        Thread.sleep(customerEntity.getDataController().getTmSystem().getPrRequestTimeout());
                    } catch (InterruptedException e) {
                        log.warn(e.getMessage(), e);
                        Thread.currentThread().interrupt();
                    }


                    String mockApiKey = customerEntity.getDataController().getTmSystem().getMockApiKey();
                    if (!Strings.isNullOrEmpty(mockApiKey) && !serviceUrl.contains("SQS")) {
                        if (Strings.isNullOrEmpty(apiKey)) {
                            logService.saveLog(responseString, "Api key is absent", serviceUrl);
                            throw new ApiKeyIsAbsentException();
                        } else if (!mockApiKey.equals(apiKey)) {
                            errorMsg = "Api-key '" + apiKey + "' is invalid";
                            logService.saveLog(responseString, errorMsg, serviceUrl);
                            throw new ApiKeyInvalidException(errorMsg);
                        }
                    }

                    customerEntity.setStatus(req.getRequestType() == RequestType.UNRESTRICT ? RequestType.NORMAL : req.getRequestType());
                    customerRepository.save(customerEntity);


                    if (customerEntity.getEmail().contains("priniterror")) {
                        logService.saveLog(responseString, "Privacy request init error emulated", serviceUrl);
                        throw new PRInitErrorException("Privacy request init error emulated");
                    } else {
                        Long timeout = customerEntity.getDataController().getTmSystem().getPrResponseTimeout();
                        logService.saveLog(responseString, "[delayed for " + timeout + " ms]", serviceUrl);

                        new java.util.Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                try {
                                    String prupdateerror = error;
                                    if (customerEntity.getEmail().contains("prupdateerror")) {
                                        prupdateerror = "Privacy update request error emulated";
                                    }
                                    deliverUpdateStatusService.deliverUpdateStatus(customerEntity.getDataController().getTmSystem(),
                                                                                   buildPrivacyRequestStatus(req, prupdateerror));
                                } catch (Exception e) {
                                    log.error("Error for request: " + req + " error: " + e.getMessage(), e);
                                }
                            }
                        }, timeout);
                    }
                });


        request.getRequests().stream().filter(req -> customerRepository.findOne(req.getCustomerIdentifier()) == null)
                .forEach(req -> {
                    String responseString = "ApiKey:" + apiKey + " -> \n" + req.toString();
                    String errorMsg = "Customer with id '" + req.getCustomerIdentifier() + "' not found";
                    logService.saveLog(responseString, errorMsg, serviceUrl);
                    sendToEverywhere(req, GlobalControllerExceptionHandler.createErrorResponse("200", errorMsg));
                });
    }

    public static PrivacyRequest convert(String in) throws ParseException {


        SimpleDateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        PrivacyRequest privacyRequest = new PrivacyRequest();
        JsonObject inJson = new JsonParser().parse(in).getAsJsonObject();
        privacyRequest.setTimestamp(dateParse.parse(inJson.get("request_batch_timestamp").getAsString()));


        List<Request> requests = new ArrayList<>();
        JsonArray jsonRequests = inJson.get("requests").getAsJsonArray();
        for (int i = 0; i < jsonRequests.size(); i++) {
            Request request = new Request();
            request.setId(UUID.fromString(jsonRequests.get(i).getAsJsonObject().get("request_id").getAsString()));
            request.setRequestType(RequestType.valueOf(jsonRequests.get(i).getAsJsonObject().get("request_type").getAsString().toUpperCase()));

            JsonElement id = jsonRequests.get(i).getAsJsonObject().get("customer_identifier");

            if (id.isJsonPrimitive()) {
                request.setCustomerIdentifier(id.getAsString().replace("\\\"", "\""));
            } else {
                request.setCustomerIdentifier(jsonRequests.get(i).getAsJsonObject().get("customer_identifier").toString());
            }

            requests.add(request);
        }

        privacyRequest.setRequests(requests);

        return privacyRequest;

    }

    public PrivacyRequestStatus buildPrivacyRequestStatus(Request request, String error) {
        PrivacyRequestStatus privacyRequestStatus = new PrivacyRequestStatus();
        StatusUpdate statusUpdate = new StatusUpdate();
        if (error == null) {
            statusUpdate.setRequestStatus(RequestState.COMPLETE);
        } else {
            statusUpdate.setRequestStatus(RequestState.FAILED);
            statusUpdate.setComment(error);
        }
        statusUpdate.setRequestId(request.getId());
        statusUpdate.setTimestamp(new Date());
        privacyRequestStatus.setStatusUpdates(Collections.singletonList(statusUpdate));
        return privacyRequestStatus;
    }

    public void sendToEverywhere(final Request request, Map<String, String> response) {
        tmSystemRepository.findAll().parallelStream().forEach(system -> {
            try {
                deliverUpdateStatusService.deliverUpdateStatus(system,
                                                               buildPrivacyRequestStatus(request, mapper.writeValueAsString(response)));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        });


    }

    public void sendToEverywhere(final String in, Map<String, String> response) throws ParseException {
        PrivacyRequest privacyRequest = PrivacyRequestService.convert(in);
        for (Request request : privacyRequest.getRequests()) {
            sendToEverywhere(request, response);
        }
    }


}
