package com.tm.emulation.service;

import com.tm.emulation.entity.CustomerEntity;
import com.tm.emulation.enums.RequestType;
import com.tm.emulation.model.Customer;
import com.tm.emulation.model.DataAdminConfig;
import com.tm.emulation.model.RetrievedData;
import com.tm.emulation.repository.CustomerRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class RetrieveCustomerDataService {
    public static final String EMAIL_PARAM = "email";
    public static final String SOURCE_PARAM = "source";
    public static final String SYSTEM_PARAM = "system";
    private final CustomerRepository customerRepository;

    public RetrievedData findByEmailAndSystem(String email, String tmSystem, String source) {

        List<CustomerEntity> customerEntity =
                customerRepository.findByEmailAndDataController_TmSystem_NameAndStatusNot(email, tmSystem, RequestType.ERASE);

        RetrievedData response = new RetrievedData();
        if (customerEntity.isEmpty()) {
            response.setEmailFoundFlag(false);
        } else {
            response.setEmailFoundFlag(true);

            response.setFoundCustomers(
                    customerEntity.stream().map(
                            cst -> Customer.builder()
                                    .latestAction(cst.getLatestAction())
                                    .latestEvent(cst.getLatestEvent())
                                    .customerIdentifier(cst.getId().trim())
                                    .personalData(cst.getPersonalData().trim())
                                    .dataAdminConfig(
                                            DataAdminConfig.builder()
                                                    .controllerId(cst.getDataController().getClientId().trim())
                                                    .controllerName(cst.getDataController().getName().trim())
                                                    .market(cst.getDataController().getMarket().trim())
                                                    .tmFlag(cst.getDataController().getTmClientFlag().trim().equalsIgnoreCase("TM"))
                                                    .build()
                                    ).build()

                    ).collect(Collectors.toList())
            );
        }

        return response;
    }

}
