package com.tm.emulation.service.privacyrequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.emulation.controller.rest.EmailRetrieveController;
import com.tm.emulation.entity.TmSystemEntity;
import com.tm.emulation.model.privacyportal.response.PrivacyRequestStatus;
import com.tm.emulation.service.DeliverUpdateStatusService.PrivacyRequestUpdateStatusDeliver;
import com.tm.emulation.service.LogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@AllArgsConstructor
public class DeliverUpdateStatusApiService implements PrivacyRequestUpdateStatusDeliver {
    private final RestTemplate restTemplate;
    private final LogService logService;
    private final ObjectMapper mapper;

    @Override
    public ResponseEntity deliver(TmSystemEntity tmSystemEntity, PrivacyRequestStatus request) throws JsonProcessingException {
        String requestAddress = tmSystemEntity.getResponseAddress();
        log.info("Sending update {} to {}", request, requestAddress);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(EmailRetrieveController.API_KEY_HEADER_NAME, tmSystemEntity.getPpApiKey());

        HttpEntity<String> apiRequest = new HttpEntity<>(mapper.writeValueAsString(request), headers);
        log.info("Sending update request {}", apiRequest);
        ResponseEntity responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(requestAddress, apiRequest, Void.class);
            logService.saveLog(requestAddress + " -> " + apiRequest, responseEntity.toString(),
                    "MOCK Updates TmSystem" + tmSystemEntity.getName());
            log.info("Update {} resulted in {}", apiRequest, responseEntity);
        } catch (RestClientResponseException e) {
            logService.saveLog(requestAddress + " -> " + apiRequest, e.getResponseBodyAsString(),
                    "MOCK Updates TmSystem" + tmSystemEntity.getName());
            log.error(e.getMessage(), e);
        }


        return responseEntity;
    }
}
