package com.tm.emulation.service.privacyrequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.emulation.entity.TmSystemEntity;
import com.tm.emulation.model.privacyportal.response.PrivacyRequestStatus;
import com.tm.emulation.service.DeliverUpdateStatusService.PrivacyRequestUpdateStatusDeliver;
import com.tm.emulation.service.LogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class DeliverUpdateStatusSqsService implements PrivacyRequestUpdateStatusDeliver {

    private final QueueMessagingTemplate queueMessagingTemplate;
    private final LogService logService;
    private final ObjectMapper mapper;


    @Override
    public ResponseEntity deliver(TmSystemEntity tmSystemEntity, PrivacyRequestStatus request) throws JsonProcessingException {
        final String responseAddress = tmSystemEntity.getResponseAddress();
        log.info("Sending update {} to {}", request, responseAddress);
        queueMessagingTemplate.convertAndSend(responseAddress, mapper.writeValueAsString(request));
        logService.saveLog("SQS:" + responseAddress + " -> " + request.toString(), "OK",
                           "MOCK updates TmSystem" + tmSystemEntity.getName());

        return ResponseEntity.ok().build();
    }
}
