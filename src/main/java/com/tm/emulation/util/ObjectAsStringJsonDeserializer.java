package com.tm.emulation.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.springframework.stereotype.Component;

@Component
public class ObjectAsStringJsonDeserializer extends JsonDeserializer<String> {
    @Override
    public String deserialize(JsonParser jsonparser,
                              DeserializationContext deserializationcontext) throws IOException {
        return jsonparser.getText();
    }

}
