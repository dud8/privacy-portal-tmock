/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `tm_system`
--
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `tm_system` (
  `ID`               BIGINT(20) NOT NULL AUTO_INCREMENT,
  `NAME`             VARCHAR(150)        DEFAULT NULL,
  `ER_TIMEOUT`       BIGINT(20) NOT NULL DEFAULT 2000,
  `PR_TIMEOUT`       BIGINT(20) NOT NULL DEFAULT 30000,
  `RESPONSE_ADDRESS` VARCHAR(200)        DEFAULT NULL,
  `MOCK_API_KEY`     VARCHAR(200)        DEFAULT '654321',
  `PP_API_KEY`       VARCHAR(200)        DEFAULT '123456',
  PRIMARY KEY (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data_controller`
--
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `data_controller` (
  `ID`           BIGINT(20)  NOT NULL      AUTO_INCREMENT,
  `TM_SYSTEM_ID` BIGINT(20)  NOT NULL,
  `MARKET`       VARCHAR(22) NOT NULL,
  `CLIENT_NAME`  VARCHAR(150)              DEFAULT NULL,
  `CLIENT_ID`    VARCHAR(150)              DEFAULT NULL,
  `CLIENT_FLAG`  ENUM ('TM', 'CLIENT')     DEFAULT 'TM',
  PRIMARY KEY (`ID`),
  CONSTRAINT `REF_MARKET` FOREIGN KEY (`TM_SYSTEM_ID`) REFERENCES `tm_system` (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `customer` (
  `IDENTIFIER`    VARCHAR(150) NOT NULL,
  `DC_ID`         BIGINT(20)   NOT NULL,
  `EMAIL`         VARCHAR(150) NOT NULL,
  `PERSONAL_DATA` VARCHAR(150)                                                         DEFAULT '{"name":"unknown"}',
  `STATUS`        ENUM ('ERASE', 'NORMAL', 'FULL_RESTRICT', 'PARTIAL_RESTRICT')        DEFAULT 'NORMAL',
  `LATEST_EVENT`  DATETIME ON UPDATE CURRENT_TIMESTAMP                                 DEFAULT CURRENT_TIMESTAMP,
  `LATEST_ACTION` DATETIME ON UPDATE CURRENT_TIMESTAMP                                 DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (`DC_ID`, `EMAIL`),
  PRIMARY KEY (`IDENTIFIER`),
  CONSTRAINT `REF_DATA_CONTROLLER` FOREIGN KEY (`DC_ID`) REFERENCES `data_controller` (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_log`
--
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `app_log` (
  `AL_ID`        BIGINT(20) NOT NULL                  AUTO_INCREMENT,
  `AL_TIMESTAMP` DATETIME ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `AL_REQUEST`   VARCHAR(1000)                        DEFAULT NULL,
  `AL_RESPONSE`  TEXT                                 DEFAULT NULL,
  `AL_SERVICE`   VARCHAR(200)                         DEFAULT NULL,
  PRIMARY KEY (`AL_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
