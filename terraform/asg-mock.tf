resource "aws_elb" "gdpr-mock" {
  name    = "${module.naming.aws_elb}"
  subnets = ["${data.terraform_remote_state.vpc.subnets_elb_private}"]

  security_groups = [
    "${data.terraform_remote_state.vpc.sg_elb_common}",
    "${aws_security_group.sg-elb-mock.id}",
  ]

  internal                    = true
  cross_zone_load_balancing   = true
  idle_timeout                = 70
  connection_draining         = true
  connection_draining_timeout = 300

  listener {
    instance_port     = 8082
    instance_protocol = "http"
    lb_port           = 8080
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 5
    target              = "HTTP:8082/"
    interval            = 30
  }

  tags {
    Name          = "${module.naming.aws_elb}"
    ProductCode   = "${var.product_code_tag}"
    Environment   = "${var.environment_tag}"
    InventoryCode = "${var.inventory_code_tag}"
  }
}

resource "aws_launch_configuration" "lc-gdpr-mockm" {
  name_prefix          = "${module.naming.aws_launch_configuration}gdpr-mock"
  image_id             = "${lookup(module.ami.ami_ids, format("%s.%s.%s", var.aws_region, var.aws_image_os, var.aws_image_version))}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.iam-inst-profile.name}"
  key_name             = "${var.key_name_prefix}"
  enable_monitoring    = "${var.lc_enable_monitoring}"
  ebs_optimized        = "${lookup(module.utils.ebs_optimized, var.instance_type, false)}"
  user_data            = "${module.ignition.ignition_config}"

  security_groups = [
    "${aws_security_group.sg-instance-mock.id}",
    "${data.terraform_remote_state.vpc.sg_linux_common}",
  ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-gdpr-mock" {
  lifecycle {
    create_before_destroy = true
  }

  name                      = "${module.naming.aws_autoscaling_group}gdpr-mock-${aws_launch_configuration.lc-gdpr-mockm.name}"
  min_size                  = "${var.asg_min}"
  max_size                  = "${var.asg_max}"
  load_balancers            = ["${aws_elb.gdpr-mock.name}"]
  launch_configuration      = "${aws_launch_configuration.lc-gdpr-mockm.name}"
  vpc_zone_identifier       = ["${data.terraform_remote_state.vpc.subnets_private_nat}"]
  health_check_grace_period = 400
  health_check_type         = "EC2"
  min_elb_capacity          = "${var.asg_min}"
  wait_for_capacity_timeout = "10m"

  enabled_metrics = ["${module.utils.asg_metrics}"]

  tag {
    key                 = "Name"
    value               = "${module.naming.aws_instance}gdpr-mock"
    propagate_at_launch = true
  }

  tag {
    key                 = "ProductCode"
    value               = "${var.product_code_tag}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "${var.environment_tag}"
    propagate_at_launch = true
  }

  tag {
    key                 = "InventoryCode"
    value               = "${var.inventory_code_tag}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "asg-scale-down-gdpr" {
  name                    = "${module.naming.aws_autoscaling_group}scale_down_gdpr"
  metric_aggregation_type = "Average"
  policy_type             = "StepScaling"

  step_adjustment {
    scaling_adjustment          = -1
    metric_interval_lower_bound = 0.0
  }

  adjustment_type        = "ChangeInCapacity"
  autoscaling_group_name = "${aws_autoscaling_group.asg-gdpr-mock.name}"
}

resource "aws_autoscaling_policy" "asg-scale-up-gdpr" {
  name                      = "${module.naming.aws_autoscaling_group}scale_up_gdpr"
  estimated_instance_warmup = 150
  metric_aggregation_type   = "Average"
  policy_type               = "StepScaling"

  step_adjustment {
    scaling_adjustment          = 40
    metric_interval_lower_bound = 0.0
  }

  adjustment_type        = "PercentChangeInCapacity"
  autoscaling_group_name = "${aws_autoscaling_group.asg-gdpr-mock.name}"
}

# Cloudwatch metric alarms
resource "aws_cloudwatch_metric_alarm" "metric-memory-high-gdpr" {
  alarm_name          = "${module.naming.aws_cloudwatch_metric_alarm}-MemoryReservationHigh"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "75"
  alarm_description   = "This metric monitors EC2 memory for High MemoryReservation on gdpr hosts"
  alarm_actions       = ["${aws_autoscaling_policy.asg-scale-up-gdpr.arn}"]

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.asg-gdpr-mock.name}"
  }

  actions_enabled = "true"
}

resource "aws_cloudwatch_metric_alarm" "metric-memory-low-gdpr" {
  alarm_name          = "${module.naming.aws_cloudwatch_metric_alarm}-MemoryReservationLow"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "25"
  alarm_description   = "This metric monitors EC2 Low MemoryReservation on gdpr hosts"
  alarm_actions       = ["${aws_autoscaling_policy.asg-scale-down-gdpr.arn}"]

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.asg-gdpr-mock.name}"
  }

  actions_enabled = "true"
}
