# This file is  used to configure the location of the Remote State file

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${var.terraformer_bucket}"
    key    = "${var.remote_state_key}"
    region = "${var.aws_region}"
  }
}