### IAM Roles and Policies
resource "aws_iam_instance_profile" "iam-inst-profile" {
  name = "${module.naming.aws_iam_instance_profile}"
  role = "${aws_iam_role.ec2-instance.name}"
}

resource "aws_iam_role" "ec2-instance" {
  name = "${module.naming.aws_iam_role}mock"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "instance-profile" {
  template = "${file("${path.module}/instance-profile-policy.json")}"

  vars {
    queue_arn = "${aws_sqs_queue.queue.arn}"
  }
}

resource "aws_iam_role_policy" "ec2-instance" {
  name   = "${module.naming.aws_iam_role_policy}"
  role   = "${aws_iam_role.ec2-instance.name}"
  policy = "${data.template_file.instance-profile.rendered}"
}
