module "naming" {
  source             = "git::https://git.tmaws.io/AWS/terraform-module-naming.git"
  product_code_tag   = "${var.product_code_tag}"
  environment_tag    = "${var.environment_tag}"
  inventory_code_tag = "${var.inventory_code_tag}"
  product_name       = "${var.product_name}"
  aws_region         = "${var.aws_region}"
  account_tag        = "${var.account_tag}"
}

module "zone" {
  source = "git::https://git.tmaws.io/intl-aws/terraform-module-zones.git"
  zone   = "${var.zone}"
}

module "ami" {
  source = "git::https://git.tmaws.io/AWS/terraform-module-ami.git"
}

module "utils" {
  source = "git::https://git.tmaws.io/intl-aws/terraform-module-utils.git"
}

module "ignition" {
  source = "./modules/ignition"

  outbound_proxy = "${var.outbound_proxy}"
  no_proxy       = "${var.no_proxy}"

  # akfgen config
  akfgen_image            = "${var.akfgen_image}"
  ldap_ssh_pub_keys_group = "${var.ldap_ssh_pub_keys_group}"
  sqs_queue               = "${aws_sqs_queue.queue.name}"
  mock_docker_tag         = "${var.mock_docker_tag}"
}
