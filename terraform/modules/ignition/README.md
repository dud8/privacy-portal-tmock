# Ignition

This module creates an Ignition-based first boot config for a CoreOS machine.

Ignition supersedes the old Cloud-Config (coreos-cloudinit) tool.

## Documentation
* Cloud-Config: https://coreos.com/os/docs/latest/cloud-config.html
* Ignition: https://coreos.com/ignition/docs/latest/
* Terraform Ignition provider: https://www.terraform.io/docs/providers/ignition/
