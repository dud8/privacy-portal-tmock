# Files to write to the machine.

# akfgen config file
data "ignition_file" "akfgen-config" {
  filesystem = "root"
  path       = "/etc/akfgen.yml"

  # Octal 0644 == Decimal 420
  mode = 420

  content {
    content = "${data.template_file.akfgen-config.rendered}"
  }
}

# authorized keys file
data "ignition_file" "authorized-keys" {
  filesystem = "root"
  path       = "/home/core/.ssh/authorized_keys.extra"

  # Octal 0644 == Decimal 420
  mode = 420

  content {
    content = "${data.template_file.authorized-keys.rendered}"
  }
}
