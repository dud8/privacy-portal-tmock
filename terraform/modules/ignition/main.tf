# Main resources for this module.

# Rendered Ignition config to be used as input to CoreOS.
data "ignition_config" "main" {
  files = [
    "${data.ignition_file.authorized-keys.id}",
    "${data.ignition_file.akfgen-config.id}",
  ]

  systemd = [
    "${data.ignition_systemd_unit.docker.id}",
    "${data.ignition_systemd_unit.update_engine.id}",
    "${data.ignition_systemd_unit.locksmithd.id}",
    "${data.ignition_systemd_unit.systemd_journal_gatewayd_socket.id}",
    "${data.ignition_systemd_unit.systemd_journal_gatewayd_service.id}",
    "${data.ignition_systemd_unit.akfgen.id}",
    "${data.ignition_systemd_unit.akfgen-timer.id}",
    "${data.ignition_systemd_unit.gdpr_mock_db.id}",
    "${data.ignition_systemd_unit.gdpr_mock_app.id}",
  ]
}
