# Module outputs.

output "ignition_config" {
  description = "Ignition config for CoreOS machine."
  value       = "${data.ignition_config.main.rendered}"
}
