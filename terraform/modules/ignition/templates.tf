# Template files to be interpolated and rendered.

# akfgen service definition
data "template_file" "akfgen" {
  template = "${file("${path.module}/units/akfgen.service")}"

  vars {
    akfgen_image = "${var.akfgen_image}"
  }
}

# akfgen config
data "template_file" "akfgen-config" {
  template = "${file("${path.module}/akfgen-conf.yml")}"

  vars {
    ldap_ssh_pub_keys_group = "${var.ldap_ssh_pub_keys_group}"
  }
}

data "template_file" "authorized-keys" {
  template = "${file("${path.module}/authorized_keys.extra")}"
}

# dnsmasq service definition.
data "template_file" "gdpr_mock_db" {
  template = "${file("${path.module}/units/gdpr-mock-db.service")}"
}

# nginx_proxy service definition.
data "template_file" "gdpr_mock_app" {
  template = "${file("${path.module}/units/gdpr-mock-app.service")}"

  vars {
    mock_docker_tag = "${var.mock_docker_tag}"
    sqs_queue       = "${var.sqs_queue}"
  }
}
