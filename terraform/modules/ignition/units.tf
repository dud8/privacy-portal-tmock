# Systemd unit definitions.

# Configure Docker daemon to use outbound proxy.
# https://docs.docker.com/config/daemon/systemd/#httphttps-proxy
data "ignition_systemd_unit" "docker" {
  name = "docker.service"

  dropin {
    name = "20-http-proxy.conf"

    content = <<EOF
[Service]
Environment="HTTP_PROXY=${var.outbound_proxy}"
Environment="NO_PROXY=${var.no_proxy}"
EOF
  }
}

# Disable automatic updates.
data "ignition_systemd_unit" "update_engine" {
  name    = "update-engine.service"
  enabled = false
  mask    = true
}

# Disable automatic reboots
data "ignition_systemd_unit" "locksmithd" {
  name    = "locksmithd.service"
  enabled = false
  mask    = true
}

# Enable remote Journald.
data "ignition_systemd_unit" "systemd_journal_gatewayd_socket" {
  name = "systemd-journal-gatewayd.socket"

  # Default value of "enabled" is true.
  # enabled = true
}

# Enable remote Journald.
data "ignition_systemd_unit" "systemd_journal_gatewayd_service" {
  name = "systemd-journal-gatewayd.service"
}

# akfgen ssh-key update tool
data "ignition_systemd_unit" "akfgen" {
  name    = "akfgen.service"
  content = "${data.template_file.akfgen.rendered}"
}

# akfgen ssh-key update tool timer
data "ignition_systemd_unit" "akfgen-timer" {
  name    = "akfgen.timer"
  content = "${file("${path.module}/units/akfgen.timer")}"
  enabled = "true"
}

data "ignition_systemd_unit" "gdpr_mock_db" {
  name    = "gdpr-mock-db.service"
  content = "${data.template_file.gdpr_mock_db.rendered}"
}

data "ignition_systemd_unit" "gdpr_mock_app" {
  name    = "gdpr-mock-app.service"
  content = "${data.template_file.gdpr_mock_app.rendered}"
}
