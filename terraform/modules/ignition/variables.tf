# Input variables for the module.
variable "outbound_proxy" {
  description = "Outbound proxy."
}

variable "no_proxy" {
  description = "Hostnames/IPs for which to bypass outbound proxy."
}

variable "akfgen_image" {
  description = "akfgen Docker image to pull"
}

variable "ldap_ssh_pub_keys_group" {
  description = "ldap ssh PRD1431 public keys group"
}

variable "mock_docker_tag" {}

variable "sqs_queue" {}
