output "instance_security_group" {
  value = "${aws_security_group.sg-instance-mock.id}"
}

output "mock_dns" {
  value = "${aws_route53_record.gdpr.fqdn}"
}

output "arn" {
  value = "${aws_sqs_queue.queue.arn}"
}

output "id" {
  value = "${aws_sqs_queue.queue.id}"
}
