resource "aws_route53_record" "gdpr" {
  zone_id = "${module.zone.zone_id}"
  name    = "gdpr.mock.${var.environment_tag}.cdmp"
  type    = "A"

  alias {
    name                   = "${lower(aws_elb.gdpr-mock.dns_name)}"
    zone_id                = "${aws_elb.gdpr-mock.zone_id}"
    evaluate_target_health = true
  }
}
