# Create a security group for ec2 instances
resource "aws_security_group" "sg-instance-mock" {
  description = "Controls access to the ASG from ELB"

  name   = "${module.naming.aws_security_group}"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.sg-elb-mock.id}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${module.utils.cidr_techops_vpn}"]
  }

  ingress {
    protocol        = "tcp"
    from_port       = 8082
    to_port         = 8082
    security_groups = ["${aws_security_group.sg-elb-mock.id}"]
  }

  # egress to es proxy
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name          = "${module.naming.aws_security_group}"
    ProductCode   = "${var.product_code_tag}"
    Environment   = "${var.environment_tag}"
    InventoryCode = "${var.inventory_code_tag}"
  }
}

# Create a security group for an ELB
resource "aws_security_group" "sg-elb-mock" {
  description = "Controls access to the application ELB"

  lifecycle {
    create_before_destroy = true
  }

  name   = "${module.naming.aws_security_group}elb"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["${module.utils.cidr_rfc1918}"]
  }

  tags {
    Name          = "${module.naming.aws_security_group}elb"
    ProductCode   = "${var.product_code_tag}"
    Environment   = "${var.environment_tag}"
    InventoryCode = "${var.inventory_code_tag}"
  }
}
