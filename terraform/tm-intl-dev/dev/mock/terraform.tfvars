#
aws_region = "eu-west-1"

# DO NOT CHANGE
aws_profile = "tm-intl-dev-Ops-Techops"

product_name = "gdpr-portal"

account_tag = "tm-intl-dev"

# Remote State file location, predefined and set up by TM
terraformer_bucket = "terraform.eu-west-1.tm-intl-dev"

remote_state_key = "PRD298/tm-intl-dev/general-ie-dev/general-ie-dev.tfstate"

# Tags to be applied to all resources (http://pim.tmaws.io/#/asset/562798?_k=j8dxud)
environment_tag = "dev"

product_code_tag = "PRD1431"

inventory_code_tag = "PPortal-mock"

# EC2
instance_type = "t2.micro"

lc_enable_monitoring = "false"

auto_scale_enabled = true

key_name_prefix = "gdpr-dev"

mock_docker_tag = "latest"

sqs_queue = "tm-intl-dev-dev-cdmp-mock"

ldap_ssh_pub_keys_group = "PRD1431-nonprod-write"

################ SQS #########################

visibility_timeout_seconds = "30"

delay_seconds = "0"

max_message_size = "262144"

message_retention_seconds = "345600"

receive_wait_time_seconds = "20"

dead_letter_queue = ""

max_receive_count = "5"
