#!/bin/bash

cat <<EOF >> /home/ec2-user/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAybkqQCfaCniFgGsFAeAyeGAElNn0qMSqLeOgWGAqTdXnzT+lphcBoaejktGKTcaHH9FFAy+JM5CWHr0fuCNOAhbE9j2QWeyhaeUt2AXQF0GNdoO/Tc0b81tibT2ELZWhmUEd+XGdNYJsh0QLCs0o0Ju9Xb3Xd4bv1w2V6bZbW8bFvwOAIwSMk7FNVMWoRxkaqhoFuz7G32tLAD54tQUp97NqJ7UDn0AStI54ih60wNZ2I9I8ryyNAMOwBcppfA8KfLUg2vIU+G8YZHzc01Pb5EA3w0VOnhrgFZlcdP+ARhI4R0T6lzhBYGvg5PlLa75mU6soG+6aP7bn14LpKyPFGQ== alona_shevliakova@epam.com
EOF

chmod 600 /home/ec2-user/.ssh/authorized_keys
# docker daemon would normally start AFTER cloud-init executes the userdata
# script so we need to start it here
/etc/init.d/docker restart

curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose

touch /root/docker-compose.yml
cat <<EOF >> /root/docker-compose.yml
version: "3.0"
services:
    db:
      environment:
        MYSQL_ROOT_PASSWORD: 123456
      image: "mariadb:latest"
      ports:
        - "3306:3306"
      restart: always
    tm-mock-service:
      environment:
        PR_INITIATE_QUEUE: ${sqs_queue}
      image: "tmhub.io/prd1431-cdmp/tm-system-backend-mock:${java_docker_tag}"
      ports:
        - "8080:8082"
      restart: always
      depends_on:
        - db
EOF
cd /root
docker-compose up -d
