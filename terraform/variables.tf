variable "availability_zones" {
  type    = "list"
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "aws_region" {}
variable "aws_profile" {}
variable "account_tag" {}
variable "product_name" {}

# TAG and name
variable "product_code_tag" {}

variable "inventory_code_tag" {}
variable "environment_tag" {}

variable "remote_state_key" {}
variable "terraformer_bucket" {}

### Autoscaling
variable "asg_min" {
  default = "1"
}

variable "asg_max" {
  default = "2"
}

variable "instance_type" {}

variable "aws_image_os" {
  default = "coreos"
}

variable "aws_image_version" {
  default = "latest"
}

variable "key_name_prefix" {}
variable "lc_enable_monitoring" {}

variable "zone" {
  default = "nonprod.tmaws.eu"
}

variable "logs_retention_period" {
  default = "30"
}

variable "outbound_proxy" {
  default = ""
}

variable "no_proxy" {
  description = "Hostnames/IPs for which to bypass outbound proxy."
  default     = "169.254.169.254,.tmcs,127.0.0.1,localhost,.tmhub.io,.tmaws.eu"
}

variable "akfgen_image" {
  default     = "tmhub.io/techops/akfgen"
  description = "akfgen Docker image to pull"
}

variable "ldap_ssh_pub_keys_group" {
  description = "ldap ssh public keys group"
}

variable "mock_docker_tag" {}

####### SQS #########

variable "visibility_timeout_seconds" {
  description = "the timeout in seconds of visibility of the message"
  default     = 30
}

variable "delay_seconds" {
  description = "delay in displaying message"
  default     = "0"
}

variable "max_message_size" {
  description = "max size of the message default to 256KB"
  default     = "262144"                                   # 256 KB
}

variable "message_retention_seconds" {
  description = "seconds of retention of the message default to 4 days"
  default     = "345600"                                                # 4 days
}

variable "receive_wait_time_seconds" {
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning. An integer from 0 to 20 (seconds). The default for this attribute is 0, meaning that the call will return immediately."
  default     = "20"
}

variable "dead_letter_queue" {
  description = "The dead letter queue to use for undeliverable messages"
  default     = ""
}

variable "max_receive_count" {
  description = "Maximum receive count"
  default     = "5"
}
